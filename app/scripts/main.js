$(document).ready(function() {
  /* $('a[href^="#"]').on('click', function (e) {
    e.preventDefault();
    var target = this.hash,
      $target = $(target);

    $('html, body').stop().animate({
      'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
      window.location.hash = target;
    });
  }); */

  $('.phone').inputmask('+7(999)999-99-99');

  $('.a-big__cards').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000
  });

  $('.a-useful__cards').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: false
        }
      }
    ]
  });

  $('.a-reviews__cards').slick({
    dots: true,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: false
        }
      }
    ]
  });

  $('.a-workers__cards').slick({
    dots: true,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: false
        }
      }
    ]
  });

  $('.a-1c__cards').slick({
    dots: true,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: false,
    autoplaySpeed: 5000,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: false
        }
      }
    ]
  });

  $('.open-modal').on('click', function(e) {
    e.preventDefault();
    $('.a-modal').slideToggle('fast', function(e) {
      // callback
    });
  });

  $('.a-modal__close').on('click', function(e) {
    e.preventDefault();
    $('.a-modal').slideToggle('fast', function(e) {
      // callback
    });
  });

  $('.a-header__mob').on('click', function(e) {
    $('.a-header__nav').slideToggle('fast', function(e) {});
    $('#mob-info').slideToggle('fast', function(e) {});
  });

  $('.d-service-aside__dropsubul').on('click', function(e) {
    if (
      !$(e.target).closest('.d-service-aside__subul').length &&
      !$(e.target).is('.d-service-aside__subul')
    ) {
      $(this)
        .next()
        .slideToggle('fast', function(e) {
          $(this)
            .prev()
            .parent()
            .toggleClass('d-service-aside__dropsubul_opened');
        });
    }
  });

  $('.d-service-aside__droplinks').on('click', function(e) {
    if (
      !$(e.target).closest('.d-service-aside__links').length &&
      !$(e.target).is('.d-service-aside__links')
    ) {
      $(this)
        .next()
        .slideToggle('fast', function(e) {
          $(this)
            .prev()
            .parent()
            .toggleClass('d-service-aside__droplinks_opened');
        });
    }
  });

  $('.d-prices__dropsubul').on('click', function(e) {
    if (
      !$(e.target).closest('.d-prices__subul').length &&
      !$(e.target).is('.d-prices__subul')
    ) {
      $(this)
        .next()
        .slideToggle('fast', function(e) {
          $(this)
            .prev()
            .parent()
            .toggleClass('d-prices__dropsubul_opened');
        });
    }
  });

  $('.d-prices__droplinks').on('click', function(e) {
    if (
      !$(e.target).closest('.d-prices__links').length &&
      !$(e.target).is('.d-prices__links')
    ) {
      $(this)
        .next()
        .slideToggle('fast', function(e) {
          $(this)
            .prev()
            .parent()
            .toggleClass('d-prices__droplinks_opened');
        });
    }
  });

  $('.d-nav__sublink-arrow_1').on('click', function(e) {
    $(this)
      .parent()
      .next()
      .slideToggle('fast');
  });

  $('.d-nav__sublink-arrow_2').on('click', function(e) {
    $(this)
      .parent()
      .next()
      .slideToggle('fast');
  });

  $('.d-nav__sublink-arrow_3').on('click', function(e) {
    $(this)
      .parent()
      .next()
      .slideToggle('fast');
  });

  $('.d-top__menu').on('click', function(e) {
    $('.d-nav').slideToggle('fast', function(e) {});
  });
});
